<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('link')->nullable();
            $table->text('description')->nullable();

            $table->boolean('generate')->default(1);
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->boolean('all_day')->default(0);
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->enum('reccurence', ['daily', 'weekly'])->default('daily');
            $table->integer('reccurence_every')->nullable();

            $table->nullableNumericMorphs('related');
            
            $table->text('meta')->nullable();



            $table->timestamps();
        });

        Schema::create('dates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->constrained()->cascadeOnDelete();
            $table->date('date');
            $table->date('date_end')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->enum('status', ['active', 'hidden'])->default('active');
            $table->text('meta')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('events');
        Schema::dropIfExists('dates');
        Schema::enableForeignKeyConstraints();
    }
}
