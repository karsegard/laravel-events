<?php

namespace KDA\Events\Database\Factories;

use KDA\Events\Models\Date;
use Illuminate\Database\Eloquent\Factories\Factory;

class DateFactory extends Factory
{
    protected $model = Date::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
