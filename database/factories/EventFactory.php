<?php

namespace KDA\Events\Database\Factories;

use KDA\Events\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    protected $model = Event::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
