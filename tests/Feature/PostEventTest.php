<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Events\Models\Event;
use KDA\Tests\Models\Post;

use KDA\Tests\TestCase;

class PostEventTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    function creates()
    {
        $post = Post::factory()->create(['title' => 'hello']);

        $event = Event::factory()->create(['title' => 'test']);
        $event->related()->associate($post)->save();
        $this->assertEquals($event->id, $post->event->id);
        $this->assertEquals($event->related->id, $post->id);
    }

    /** @test */
    function post_has_dates()
    {
        $event = Event::factory()->create(['title' => 'test', 'generate' => true, 'start' => '2022-06-01', 'end' => '2022-06-31', 'reccurence' => 'daily', 'time_start' => '19:00']);
        $event->regenerateDates();
       
        $post = Post::factory()->create(['title' => 'hello']);
        $post->event()->save($event);
        $this->assertEquals($event->id, $post->event->id);
        $this->assertEquals($event->related->id, $post->id);
        $this->assertEquals($post->dates->count(), 31);
    }


    /** @test */
    function post_has_no_hidden_dates()
    {
        $event = Event::factory()->create(['title' => 'test', 'generate' => true, 'start' => '2022-06-01', 'end' => '2022-06-31', 'reccurence' => 'daily', 'time_start' => '19:00']);
        $event->regenerateDates();
        $date =  $event->dates->get(0);
        $date->status='hidden';
        $date->save();
        $post = Post::factory()->create(['title' => 'hello']);
        $post->event()->save($event);
        $this->assertEquals($event->id, $post->event->id);
        $this->assertEquals($event->related->id, $post->id);
        $this->assertEquals($post->dates->count(), 30);
    }
}
