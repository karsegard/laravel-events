<?php

namespace KDA\Events\Models\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;
use KDA\Events\Models\Date;
use KDA\Events\Models\Event;
trait IsEvent
{


    public static function bootIsEvent()
    {

        
    }

    public function event()
    {
        return $this->morphOne(Event::class, 'related');
    }

    public function dates()
    {
        return $this->hasManyThrough(Date::class, Event::class,'related_id')->where(
            'related_type', 
            array_search(static::class, Relation::morphMap()) ?: static::class
        )->where('dates.status','!=','hidden');
    }

/*
    public function currentSlug()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
