<?php

namespace KDA\Events\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Date extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
        'date_end',
        'status',
        'meta',
        'time_start',
        'time_end',
        'event_id'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       'meta'=>'array',
       'date'=>'date',
       'date_end'=>'date',
       'time_start'=>'datetime',
       'time_end'=>'datetime',
       'meta'=>'array'
    ];
    protected $fakeColumns = ['meta'];

   
    protected static function newFactory()
    {
        return  \KDA\Events\Database\Factories\DateFactory::new();
    }

    public function scopeForEvent($query,$event_id){
        return $query->where('event_id',$event_id);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

}
