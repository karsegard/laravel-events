<?php

namespace KDA\Events\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'start',
        'end',
        'time_start',
        'time_end',
        'reccurence',
        'all_day',
        'generate'
    ];


    protected $casts = [
        'start' => 'date',
        'end' => 'date',
        'time_start' => 'datetime',
        'time_end' => 'datetime',
        'all_day'=>'boolean',
        'generate'=>'boolean'
    ];


    protected static function newFactory()
    {
        return  \KDA\Events\Database\Factories\EventFactory::new();
    }

    public function dates()
    {
        return $this->hasMany(Date::class);
    }

    public function from(){
        return $this->hasOne(Date::class)->ofMany('date','min');
    }

    public function until(){
        return $this->hasOne(Date::class)->ofMany('date','max');
    }
    
    public function related(){
        return $this->morphTo();
    }

    public function regenerateDates()
    {
        $time_start = (new \DateTime($this->time_start));
        $time_end = (new \DateTime($this->time_end));
        
        if ($this->generate ) {
          
            $date  = clone $this->start;
            $date_end  = clone $this->end;

            $interval = $this->reccurence == 'daily' ? 'day' : 'week';

            $dates  = CarbonPeriod::create($date, '1 ' . $interval, $date_end);
        
            collect($dates->toArray())->map(
                function ($item) use ($time_start,$time_end) {
                    $existing = Date::where('event_id', $this->id)->where('date', $item)->get()->count();
                    if ($existing == 0) {
                        $r = new Date;
                        $r->event_id = $this->id;
                        $r->date = $item;
                        if (!$this->all_day) {
                            $r->time_start = $time_start->format('H:i');
                            $r->time_end = $time_end->format('H:i');
                        }
                        $r->save();
                    }
                }
            );
        }/*else if(!$this->dates || $this->dates->count()==0){
            
            Date::where('event_id', $this->id)->delete();
            $r = new Date;
            $r->event_id = $this->id;
            $r->date = $this->start;
            if (!$this->all_day) {
                $r->time_start = $time_start->format('H:i');
                $r->time_end = $time_end->format('H:i');
            }
            $r->save();

        }*/
    }

    public function getEventTitleAttribute(){
        if($this->related){
            return $this->related->event_title ??  $this->title ??'event_title attribute not set';
        }else{
            return $this->title;
        }
    }

   
}
